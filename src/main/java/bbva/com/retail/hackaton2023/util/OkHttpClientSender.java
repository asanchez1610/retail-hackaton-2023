package bbva.com.retail.hackaton2023.util;
import bbva.com.retail.hackaton2023.dto.OauthResponseDTO;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
@Slf4j
public class OkHttpClientSender {
    public static Response execute(RequestBody body, String url, String method, OauthResponseDTO oauthData) {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        String authorization =  oauthData.getToken_type().concat(" ").concat(oauthData.getAccess_token());
        log.info("Authorization {}", authorization);
        Request request = new Request.Builder()
                .url(url)
                .method(method, body)
                .addHeader("Authorization", authorization)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    public static TrustManager[] getTrutsManager() {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new java.security.cert.X509Certificate[]{};
                    }
                }
        };
        return trustAllCerts;
    }

    public static Response executeWithOffCertificate(String bodyStr, String url, String method, String trxCode) throws RetailException {
        try {
            TrustManager[] trustAllCerts = OkHttpClientSender.getTrutsManager();
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            OkHttpClient.Builder newBuilder = new OkHttpClient.Builder();
            newBuilder.sslSocketFactory(sslContext.getSocketFactory(), (X509TrustManager) trustAllCerts[0]);
            newBuilder.hostnameVerifier((hostname, session) -> true);
            OkHttpClient client = newBuilder.build();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, bodyStr);
            Request request = new Request.Builder()
                    .url(url)
                    .method(method, body)
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json")
                    .addHeader("header-name", "QP06")
                    .addHeader("logical-transaction-code", trxCode)
                    .addHeader("type-code", "15")
                    .addHeader("subtype-code", "17")
                    .addHeader("version-code", "01")
                    .addHeader("country-code", "PE")
                    .addHeader("entity-code", "0011")
                    .addHeader("branch-code", "0486")
                    .addHeader("workstation-code", "W963")
                    .addHeader("operative-entity-code", "0011")
                    .addHeader("operative-branch-code", "0486")
                    .addHeader("ip-address", "118.222.161.30")
                    .addHeader("channel-code", "01")
                    .addHeader("environ-code", "01")
                    .addHeader("product-code", "0001")
                    .addHeader("language-code", "ES")
                    .addHeader("user-code", "ZG13001")
                    .addHeader("operation-date", "20210105")
                    .addHeader("operation-time", "103000")
                    .addHeader("currency-code", "PEN")
                    .addHeader("secondary-currency-code", "604")
                    .addHeader("request-id", "PGLUT002-01-PEd")
                    .addHeader("service-id", "012345678901234")
                    .addHeader("client-identification-type", "L")
                    .addHeader("client-document", "09313658")
                    .addHeader("contact-identifier", "29071967")
                    .addHeader("authorization-version", "09")
                    .addHeader("authorization-code", "15172027")
                    .addHeader("accounting-terminal", "001")
                    .addHeader("logical-terminal", "01")
                    .addHeader("sequence-id", "20")
                    .addHeader("aap", "ether")
                    .addHeader("app", "ether-apx")
                    .addHeader("origin", "Onboarding")
                    .addHeader("agent-user", "p028036")
                    .addHeader("manager-user", "p028036")
                    .addHeader("mac", "E8-6A-64-F7-B007")
                    .addHeader("remaining-time", "235959")
                    .build();

            Response response = client.newCall(request).execute();
            return response;
        } catch (Exception e) {
            log.error("Error al consultar el servico APX Causa: ", e.getCause());
            log.error("Error al consultar el servico APX Message: ", e.getMessage());
            long code = 400;
            throw new RetailException("Error al consultar el servicio", code);
        }
    }
}
