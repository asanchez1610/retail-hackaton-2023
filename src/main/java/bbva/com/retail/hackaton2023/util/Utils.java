package bbva.com.retail.hackaton2023.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
public class Utils {
	
    public static List<Map<String, Object>> buildError(RetailException ex) {
        Map<String, Object> errorMap = new HashMap<>();
        errorMap.put("code", ex.getCode().toString());
        errorMap.put("message", ex.getMessage());
        errorMap.put("parameters", new ArrayList<>());
        errorMap.put("type", "WARNING");
        List<Map<String, Object>> errors = new ArrayList<>();
        errors.add(errorMap);
        return  errors;
    }

    public static String objectToStringJson(Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        return  json;
    }
    
    public static byte[] objectToBytes(Object object) {
    	ObjectMapper mapper = new ObjectMapper();
    	try {
			return mapper.writeValueAsBytes(object);
		} catch (JsonProcessingException e) {
			return null;
		}
    }

}
