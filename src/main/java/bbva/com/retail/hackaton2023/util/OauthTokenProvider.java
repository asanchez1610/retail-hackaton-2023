package bbva.com.retail.hackaton2023.util;

import bbva.com.retail.hackaton2023.dto.OauthResponseDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@Component
@Slf4j
public class OauthTokenProvider {
    /** Properties Wavetec **/
    @Value("${retail.oauth.grantType}")
    private String grantType;
    @Value("${retail.oauth.clientId.wavetec}")
    private String clientIdWavetec;
    @Value("${retail.oauth.clientSecret.wavetec}")
    private String clientSecretWavetec;
    @Value("${retail.oauth.path.wavetec}")
    private String pathValueWavetec;
    @Value("${retail.digital.apppointments.host.wavetec}")
    private String hostValueWavetec;
    /** Properties Hiper **/
    @Value("${retail.oauth.clientId.hiper}")
    private String clientIdHiper;
    @Value("${retail.oauth.clientSecret.hiper}")
    private String clientSecretHiper;
    @Value("${retail.oauth.path.hiper}")
    private String pathValueHiper;
    @Value("${retail.digital.apppointments.host.hiper}")
    private String hostValueHiper;
    
    public OauthResponseDTO generate(TicketManager type) throws IOException, RetailException {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        boolean isWavetec = TicketManager.WAVETEC == type;
        final String clientId = isWavetec ? clientIdWavetec : clientIdHiper;
        final String clientSecret = isWavetec ? clientSecretWavetec : clientSecretHiper;
        final String hostValue = isWavetec ? hostValueWavetec : hostValueHiper;
        final String pathValue = isWavetec ? pathValueWavetec : pathValueHiper;
        log.info("grant_type = {}", grantType);
        log.info("client_id = {}", clientId);
        log.info("client_secret = {}", clientSecret);
        Charset charset = Charset.forName(StandardCharsets.UTF_8.name());
        RequestBody body = new FormBody.Builder(charset)
                .add("grant_type", grantType)
                .add("client_id", clientId)
                .add("client_secret", clientSecret)
                .build();
        Request request = new Request.Builder()
                .url(hostValue.concat(pathValue))
                .post(body)
                .addHeader("Accept", "*/*")
                .addHeader("Accept-Encoding", "gzip, deflate, br")
                .addHeader("Connection", "keep-alive")
                .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .build();
        ObjectMapper objectMapper = new ObjectMapper();
        Response response = client.newCall(request).execute();
        ResponseBody responseBody = response.body();
        String strResponse = responseBody.string();
        log.info("responseBody = {}", strResponse);
        OauthResponseDTO oauthResponse =  objectMapper.readValue(strResponse, OauthResponseDTO.class);
        oauthResponse.setTicketManager(type);
        log.info("oauthResponse.getStatus() = {}", oauthResponse.getStatus());
        log.info("code result = {}", response.code());
        if  (isWavetec && oauthResponse.getStatus() != 200) {
            throw new RetailException(oauthResponse.getMessage(), oauthResponse.getErrorCode());
        } else if (response.code() != 200) {
            throw new RetailException("Error al generar token en hiper", (long) response.code());
        }
        return oauthResponse;
    }

}
