package bbva.com.retail.hackaton2023.util;
import lombok.Data;
@Data
public class RetailException extends Exception{
    private static final long serialVersionUID = 1L;
    protected Long code;
    public RetailException(String message) {
        super(message);
    }
    public RetailException(String message, Long code) {
        super(message);
        this.code = code;
    }
}
