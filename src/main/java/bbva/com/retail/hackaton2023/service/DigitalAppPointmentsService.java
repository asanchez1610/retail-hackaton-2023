package bbva.com.retail.hackaton2023.service;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import bbva.com.retail.hackaton2023.dto.OauthResponseDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataAvailableDateDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataCancelationDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataDigitalAppointmentDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataListAvailableScheduleDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataListDigitalAppointmentDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataOperativeTypeDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DigitalAppointmentDTO;
import bbva.com.retail.hackaton2023.dto.pointments.ResponseApxOperativeTypeAPX;
import bbva.com.retail.hackaton2023.util.OauthTokenProvider;
import bbva.com.retail.hackaton2023.util.OkHttpClientSender;
import bbva.com.retail.hackaton2023.util.RetailException;
import bbva.com.retail.hackaton2023.util.TicketManager;
import bbva.com.retail.hackaton2023.util.Utils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

@Service
@Slf4j
public class DigitalAppPointmentsService {

    public static final String HIPER = "h";
    public static final String WAVETEC  = "w";
    public static final String HIPER_VALUE = "MANAGER_TICKET_HIPER";
    public static final String WAVETEC_VALUE  = "MANAGER_TICKET_WAVETEC";
    public static final long ERROR_CODE = 400;
    private static final String PATH_POIS_JSON  = "mock/POIS.json";

    @Value("${retail.digital.apppointments.host.wavetec}")
    private String hostValueWavetec;
    @Value("${retail.oauth.apppointments.paths.getoperativetype.wavetec}")
    private String pathGtOperativeTypeWavetec;
    
    @Value("${retail.oauth.apppointments.paths.getavailabledates.wavetec}")
    private String pathGetAvailableDatesWavetec;
    
    @Value("${retail.oauth.apppointments.paths.listavailableschedules.wavetec}")
    private String pathListAvailableSchedulesWavetec;
    
    @Value("${retail.oauth.apppointments.paths.createdigitalappointment.wavetec}")
    private String pathCreateDigitalAppointmentWavetec;
    
    @Value("${retail.oauth.apppointments.paths.getdigitalappointment.wavetec}")
    private String pathGetDigitalAppointmentWavetec;
    
    @Value("${retail.oauth.apppointments.paths.deletedigitalappointment.wavetec}")
    private String pathDeleteDigitalAppointmentWavetec;
    
    @Value("${retail.digital.apppointments.host.hiper}")
    private String hostValueHiper;
    
    @Value("${retail.oauth.apppointments.paths.getoperativetype.hiper}")
    private String pathGtOperativeTypeHiper;
    
    @Value("${retail.oauth.apppointments.paths.getavailabledates.hiper}")
    private String pathGetAvailableDatesHiper;
    
    @Value("${retail.oauth.apppointments.paths.listavailableschedules.hiper}")
    private String pathListAvailableSchedulesHiper;
    
    @Value("${retail.oauth.apppointments.paths.createdigitalappointment.hiper}")
    private String pathCreateDigitalAppointmentHiper;
    
    @Value("${retail.oauth.apppointments.paths.getdigitalappointment.hiper}")
    private String pathGetDigitalAppointmentHiper;
    
    @Value("${retail.oauth.apppointments.paths.deletedigitalappointment.hiper}")
    private String pathDeleteDigitalAppointmentHiper;

    @Value("${retail.apx.trx.operativeType}")
    private String trxCodeOperativeType;

    @Value("${retail.apx.url}")
    private String urlApxService;

    @Autowired
    private OauthTokenProvider oauthTokenProvider;
    
    private ObjectMapper objectMapper = new ObjectMapper();

    private OauthResponseDTO createCredentials(String branchId) throws RetailException {
        try {
            return oauthTokenProvider.generate(this.bifurcateService(branchId));
        } catch (Exception e) {
            throw new RetailException("Error al generar las credenciales", ERROR_CODE);
        }
    }

    private InputStream getFileFromResourceAsStream(String fileName) throws RetailException {
        log.info("getFileFromResourceAsStream fileName = {}", fileName);
        ClassLoader classLoader = getClass().getClassLoader();
        URL url = classLoader.getResource(fileName);
        File file = new File(url.getFile());
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            throw new RetailException("File Json POIs not found", DigitalAppPointmentsService.ERROR_CODE);
        }
        return inputStream;
    }

    @SuppressWarnings("unchecked")
	private TicketManager bifurcateService(String _branchId) throws RetailException {
        if (_branchId == null) {
            return TicketManager.WAVETEC;
        }
        final String branchId = _branchId.replace("PE", "");
        InputStream is = this.getFileFromResourceAsStream(DigitalAppPointmentsService.PATH_POIS_JSON);
        try {
            Map<String, Object> data = objectMapper.readValue(is, Map.class);
            List<Map<String, Object>> pois = (List<Map<String, Object>>) data.get("data");
            log.info("pois length = {}", pois.size());
            Map<String, Object> resultPoi = pois.stream().filter(poiDTO -> poiDTO.get("id").toString().equals(branchId)).findFirst().get();
            if (resultPoi == null) {
                return TicketManager.WAVETEC;
            }
            List<Map<String, String>> availableServiceDTOS = (List<Map<String, String>>) resultPoi.get("availableServices");
            log.info("availableServiceDTOS length = {}", availableServiceDTOS.size());
            boolean isHiper = availableServiceDTOS.stream().anyMatch(availableServiceDTO -> DigitalAppPointmentsService.HIPER_VALUE.equalsIgnoreCase(availableServiceDTO.get("id")));
            log.info("availableServiceDTOS value = {}", availableServiceDTOS.get(0).get("id"));
            log.info("ISHIPER- = {}", isHiper);
            if (isHiper) {
                return TicketManager.HIPER;
            }
            return TicketManager.WAVETEC;

        } catch (IOException e) {

            throw new RetailException("File Json POIs not found", DigitalAppPointmentsService.ERROR_CODE);
        }
    }

    public DataOperativeTypeDTO getOperativeTypes(String branchId) throws RetailException {
        OauthResponseDTO credentials = this.createCredentials(branchId);
        boolean isWavetec = TicketManager.WAVETEC == credentials.getTicketManager();
        final String hostValue = isWavetec ? hostValueWavetec : hostValueHiper;
        final String pathGtOperativeType = isWavetec ? pathGtOperativeTypeWavetec : pathGtOperativeTypeHiper;
        String url = hostValue.concat(pathGtOperativeType);
        url = url.replace("{branchId}", branchId);
        log.info("URL getOperativeTypes = {}", url);
        log.info("Credentials access_token = {}", credentials.getAccess_token());
        Response response = OkHttpClientSender.execute(null, url, "GET", credentials);
        ResponseBody responseBody = response.body();
        DataOperativeTypeDTO operativeTypes = null;
        try {
            String strBody = responseBody.string();
            log.info("Response getOperativeTypes = {}", strBody);
            if (strBody == null || strBody.length() == 0) {
                long code = 204;
                throw new RetailException("No se encontraron resultados", code);
            }
            if (response.code() == 400) {
                OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                throw new RetailException(responseError.getMessage(), responseError.getErrorCode());
            } if (strBody.indexOf("requestId") >= 0) {
                OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                throw new RetailException("Error - path: ".concat(responseError.getPath()).concat(", requestId: ").concat(responseError.getRequestId()), ERROR_CODE);
            } else {
                operativeTypes = objectMapper.readValue(strBody, DataOperativeTypeDTO.class);
            }
        } catch (IOException e) {
            log.error("Error Service {}", e.getMessage());
            long code = 500;
            throw new RetailException("Error inesperado", code);
        }
        return operativeTypes;
    }

    public DataOperativeTypeDTO getOperativeTypesApxOnline(String branchId) throws RetailException {
        OauthResponseDTO credentials = this.createCredentials(branchId);
        String serviceType = TicketManager.WAVETEC == credentials.getTicketManager() ? "W" : "H";
        log.info("serviceType getOperativeTypesApxOnline = {}", serviceType);

        try {
            Map<String, Object> mapBody = new HashMap<>();
            mapBody.put("branch-id", branchId);
            mapBody.put("token", credentials.getAccess_token());
            mapBody.put("serviceType", serviceType);

            Response response = OkHttpClientSender.executeWithOffCertificate(
                    Utils.objectToStringJson(mapBody),
                    urlApxService,
                    "POST",
                    trxCodeOperativeType );

            String strBody = response.body().string();
            log.info("Body response {}", strBody);
            if (strBody.length() == 0) {
                long code = 204;
                throw new RetailException("No se encontraron resultados", code);
            }
            ResponseApxOperativeTypeAPX operativeTypes = objectMapper.readValue(strBody, ResponseApxOperativeTypeAPX.class);
            return  operativeTypes.getData();
        } catch (Exception e) {
            log.error("Error al consultar el servico APX Causa: ", e.getCause());
            log.error("Error al consultar el servico APX Message: ", e.getMessage());
            long code = 400;
            throw new RetailException("Error al consultar el servicio", code);
        }
    }
    
    public DataAvailableDateDTO getAvailableDates(String branchId, Optional<String> operativeTypeId, Optional<String> attentionType) throws RetailException {
        OauthResponseDTO credentials = this.createCredentials(branchId);
        boolean isWavetec = TicketManager.WAVETEC == credentials.getTicketManager();
        
        String param = "?";
        if (operativeTypeId.isPresent()) {
        	String paramOperativeType = isWavetec ? "typeid=" : "operativeType.id=";
        	param = param.concat(paramOperativeType).concat(operativeTypeId.get());
        }   
        final String host = isWavetec ? hostValueWavetec : hostValueHiper;
        final String path = isWavetec ? pathGetAvailableDatesWavetec : pathGetAvailableDatesHiper;
        String url = host.concat(path).replace("{branchId}", branchId).concat(param);
        
        log.info("URL getAvailableDates = {}", url);
        log.info("Credentials access_token = {}", credentials.getAccess_token());
        Response response = OkHttpClientSender.execute(null, url, "GET", credentials);
        ResponseBody responseBody = response.body();
        DataAvailableDateDTO availableDate = null;
        try {
            String strBody = responseBody.string();
            log.info("Response getAvailableDates = {}", strBody);
            if (strBody == null || strBody.length() == 0) {
                long code = 204;
                throw new RetailException("No se encontraron resultados", code);
            }
            if (response.code() == 400) {
                OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                throw new RetailException(responseError.getMessage(), responseError.getErrorCode());
            } if (strBody.indexOf("requestId") >= 0) {
                OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                throw new RetailException("Error - path: ".concat(responseError.getPath()).concat(", requestId: ").concat(responseError.getRequestId()), ERROR_CODE);
            } else {
            	availableDate = objectMapper.readValue(strBody, DataAvailableDateDTO.class);
            }
        } catch (IOException e) {
            log.error("Error Service {}", e.getMessage());
            long code = 500;
            throw new RetailException("Error inesperado", code);
        }
        return availableDate;
    }
    
    
    public DataListAvailableScheduleDTO listAvailableSchedules(String branchId, String availableDateId, Optional<String> operativeTypeId) throws RetailException {
    	OauthResponseDTO credentials = this.createCredentials(branchId);
        boolean isWavetec = TicketManager.WAVETEC == credentials.getTicketManager();
        final String hostValue = isWavetec ? hostValueWavetec : hostValueHiper;
        final String pathListAvailableSchedules = isWavetec ? pathListAvailableSchedulesWavetec: pathListAvailableSchedulesHiper;
        
        String param = "?";
        if (operativeTypeId.isPresent()) {
        	String paramOperativeType = isWavetec ? "typeid=" : "operativeType.id=";
        	param = param.concat(paramOperativeType).concat(operativeTypeId.get());
        }   
        String url = hostValue.concat(pathListAvailableSchedules).concat(param);
        url = url.replace("{branchId}", branchId);
        url = url.replace("{availableDate}", availableDateId);
        
        log.info("URL listAvailableSchedules = {}", url);
        log.info("Credentials access_token = {}", credentials.getAccess_token());
        Response response = OkHttpClientSender.execute(null, url, "GET", credentials);
        ResponseBody responseBody = response.body();
        DataListAvailableScheduleDTO availableShedules = null;
        try {
            String strBody = responseBody.string();
            log.info("Response listAvailableSchedules = {}", strBody);
            if (strBody == null || strBody.length() == 0) {
                long code = 204;
                throw new RetailException("No se encontraron resultados", code);
            }
            if (response.code() == 400) {
                OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                throw new RetailException(responseError.getMessage(), responseError.getErrorCode());
            } if (strBody.indexOf("requestId") >= 0) {
                OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                throw new RetailException("Error - path: ".concat(responseError.getPath()).concat(", requestId: ").concat(responseError.getRequestId()), ERROR_CODE);
            } else {
            	availableShedules = objectMapper.readValue(strBody, DataListAvailableScheduleDTO.class);
            }
        } catch (IOException e) {
            log.error("Error Service {}", e.getMessage());
            long code = 500;
            throw new RetailException("Error inesperado", code);
        }
        return availableShedules;
    }
    
    @SuppressWarnings("deprecation")
    public DataDigitalAppointmentDTO createDigitalAppointment(DigitalAppointmentDTO body) throws RetailException {
    	OauthResponseDTO credentials = this.createCredentials(body.getBank().getBranch().getId());
        boolean isWavetec = credentials.getTicketManager() == TicketManager.WAVETEC;
        final String hostValue = isWavetec ? hostValueWavetec : hostValueHiper;
        final String pathGtCreateDigitalAppointment = isWavetec ? pathCreateDigitalAppointmentWavetec : pathCreateDigitalAppointmentHiper;
        String url = hostValue.concat(pathGtCreateDigitalAppointment);;
        
        log.info("URL createDigitalAppointment = {}", url);
        log.info("Credentials access_token = {}", credentials.getAccess_token());
        MediaType mediaType = MediaType.parse("application/json");
		RequestBody requestBody = RequestBody.create(mediaType, Utils.objectToStringJson(body));
        
        Response response = OkHttpClientSender.execute(requestBody, url, "POST", credentials);
        ResponseBody responseBody = response.body();
        DataDigitalAppointmentDTO digitalAppointment = null;
        try {
            String strBody = responseBody.string();
            log.info("Response createDigitalAppointment = {}", strBody);
            if (strBody == null || strBody.length() == 0) {
                long code = 204;
                throw new RetailException("No se encontraron resultados", code);
            }
            if (response.code() == 400) {
                OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                throw new RetailException(responseError.getMessage(), responseError.getErrorCode());
            } if (strBody.indexOf("requestId") >= 0) {
                OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                throw new RetailException("Error - path: ".concat(responseError.getPath()).concat(", requestId: ").concat(responseError.getRequestId()), ERROR_CODE);
            } else {
            	digitalAppointment = objectMapper.readValue(strBody, DataDigitalAppointmentDTO.class);
            }
        } catch (IOException e) {
            log.error("Error Service {}", e.getMessage());
            long code = 500;
            throw new RetailException("Error inesperado", code);
        }
        return digitalAppointment;
    }
    
    public DataListDigitalAppointmentDTO listDigitalAppointment(String customerId) throws RetailException {
    	DataListDigitalAppointmentDTO digitalAppointments = null;
    	
    	for (int i = 0; i < 2; i++) {
    		OauthResponseDTO credentials = null;
    		try {
    			credentials = oauthTokenProvider.generate(i == 0 ? TicketManager.WAVETEC : TicketManager.HIPER);
    		} catch (Exception e) {
    			throw new RetailException("Error al generar las credenciales", ERROR_CODE);
    		}
    		boolean isWavetec = credentials.getTicketManager() == TicketManager.WAVETEC;
            final String hostValue = isWavetec ? hostValueWavetec : hostValueHiper;
            final String pathGtGetDigitalAppointment = isWavetec ? pathGetDigitalAppointmentWavetec : pathGetDigitalAppointmentHiper;
            String url = hostValue.concat(pathGtGetDigitalAppointment).replace("{customerId}", customerId);
            
            log.info("URL listDigitalAppointment = {}", url);
            log.info("Credentials access_token = {}", credentials.getAccess_token());
            Response response = OkHttpClientSender.execute(null, url, "GET", credentials);
            ResponseBody responseBody = response.body();
            try {
                String strBody = responseBody.string();
                log.info("Response listDigitalAppointment = {}", strBody);
                if (strBody == null || strBody.length() == 0) {
                	if (i == 0) {
                		continue;
                	} else {
                		long code = 204;
                        throw new RetailException("No se encontraron resultados", code);
                	}
                }
                if (response.code() == 400) {
                    OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                    throw new RetailException(responseError.getMessage(), responseError.getErrorCode());
                } if (strBody.indexOf("requestId") >= 0) {
                    OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                    throw new RetailException("Error - path: ".concat(responseError.getPath()).concat(", requestId: ").concat(responseError.getRequestId()), ERROR_CODE);
                } else {
                	digitalAppointments = objectMapper.readValue(strBody, DataListDigitalAppointmentDTO.class);
                	break;
                }
            } catch (IOException e) {
                log.error("Error Service {}", e.getMessage());
                long code = 500;
                throw new RetailException("Error inesperado", code);
            }
    	}
    	return digitalAppointments;
    }
    
    public DataCancelationDTO deleteDigitalAppointment(String digitalAppointmentId) throws RetailException {
    	DataCancelationDTO cancelation = null;
    	
    	for (int i = 0; i < 2; i++) {
    		OauthResponseDTO credentials = null;
    		try {
    			credentials = oauthTokenProvider.generate(i == 0 ? TicketManager.WAVETEC : TicketManager.HIPER);
    		} catch (Exception e) {
    			throw new RetailException("Error al generar las credenciales", ERROR_CODE);
    		}
    		boolean isWavetec = credentials.getTicketManager() == TicketManager.WAVETEC;
            final String hostValue = isWavetec ? hostValueWavetec : hostValueHiper;
            final String pathGtDeleteDigitalAppointment = isWavetec ? pathDeleteDigitalAppointmentWavetec : pathDeleteDigitalAppointmentHiper;
            String url = hostValue.concat(pathGtDeleteDigitalAppointment).replace("{digitalAppointmentId}", digitalAppointmentId);
            
            log.info("URL deleteDigitalAppointment = {}", url);
            log.info("Credentials access_token = {}", credentials.getAccess_token());
            Response response = OkHttpClientSender.execute(null, url, isWavetec ? "GET" : "DELETE", credentials);
            ResponseBody responseBody = response.body();
            try {
                String strBody = responseBody.string();
                log.info("Response deleteDigitalAppointment = {}", strBody);
                if (response.code() == 400) {
                	if (strBody.indexOf("no existe") >= 0 && isWavetec) {
                		continue;
                	} else {
                		OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                        throw new RetailException(responseError.getMessage(), responseError.getErrorCode());
                	}                    
                } 
                if (strBody.indexOf("requestId") >= 0) {
                    OauthResponseDTO responseError = objectMapper.readValue(strBody, OauthResponseDTO.class);
                    throw new RetailException("Error - path: ".concat(responseError.getPath()).concat(", requestId: ").concat(responseError.getRequestId()), ERROR_CODE);
                } else {
                	cancelation = objectMapper.readValue(strBody, DataCancelationDTO.class);
                	break;
                }
            } catch (IOException e) {
                log.error("Error Service {}", e.getMessage());
                long code = 500;
                throw new RetailException("Error inesperado", code);
            }
    	}
    	return cancelation;
    }
}
