package bbva.com.retail.hackaton2023.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class UploadController {

    @PostMapping("/upload")
    public ResponseEntity<?> uploadOne(@RequestParam("file") MultipartFile file ) {

        String fileName = file.getOriginalFilename();
        Map<String, Object> map = new HashMap<>();
        try {
            map.put("fileName", fileName);
            map.put("filseSize", file.getSize());
            map.put("contentType", file.getContentType());
            return ResponseEntity.ok(map);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

    @PostMapping("/upload-array")
    public ResponseEntity<?> uploadArray(
            @RequestParam("files") MultipartFile[] files,
            @RequestHeader(name = "test-header", required = false) String testHeader,
            @RequestParam("paramValue") String paramValue) {
        List<Map<String, Object>> mapFiles = new ArrayList<>();
        Map<String, Object> map;
        if (files != null && files.length > 0) {
            for (MultipartFile file: files
                 ) {
                map = new HashMap<>();
                map.put("fileName",  file.getOriginalFilename());
                map.put("filseSize", file.getSize());
                map.put("contentType", file.getContentType());
                map.put("testHeader", testHeader);
                map.put("paramValue", paramValue);
                mapFiles.add(map);
            }
        }
        try {
            return ResponseEntity.ok(mapFiles);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

    }

}
