package bbva.com.retail.hackaton2023.controller;
import bbva.com.retail.hackaton2023.dto.pointments.DataAvailableDateDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataListAvailableScheduleDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataOperativeTypeDTO;
import bbva.com.retail.hackaton2023.service.DigitalAppPointmentsService;
import bbva.com.retail.hackaton2023.util.RetailException;
import bbva.com.retail.hackaton2023.util.Utils;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/branches/v0/branches")
public class BranchesController {

    @Autowired
    private DigitalAppPointmentsService digitalAppPointmentsService;

    @GetMapping("{branch-id}/digital-appointments/operative-types")
    public ResponseEntity<?> getOperativeTypes(@PathVariable("branch-id") String branchId) {
        try {
            DataOperativeTypeDTO data = digitalAppPointmentsService.getOperativeTypesApxOnline(branchId);
            return new ResponseEntity<>(data, HttpStatus.OK);
        }catch (RetailException e) {
            return new ResponseEntity<>(Utils.buildError(e), HttpStatus.CONFLICT);
        }
    }
    
    @GetMapping("{branch-id}/digital-appointments/available-dates")
    public ResponseEntity<?> getBranchesDigitalAppointmentsAvailableDates(@PathVariable("branch-id") String branchId,
    		@RequestParam("operativeType.id") Optional<String> operativeTypeId,
    		@RequestParam("operativeType.attentionType") Optional<String> attentionType) {
        try {
        	DataAvailableDateDTO data = digitalAppPointmentsService.getAvailableDates(branchId, operativeTypeId, attentionType);
            return new ResponseEntity<>(data, HttpStatus.OK);
        }catch (RetailException e) {
            return new ResponseEntity<>(Utils.buildError(e), HttpStatus.CONFLICT);
        }
    }

    @GetMapping("{branch-id}/digital-appointments/available-dates/{available-date-id}/available-schedules")
    public ResponseEntity<?> listAvailableSchedules(@PathVariable("branch-id") String branchId,
    		@PathVariable("available-date-id") String availableDateId,
    		@RequestParam("operativeType.id") Optional<String> operativeTypeId) {
        try {
        	DataListAvailableScheduleDTO data = digitalAppPointmentsService.listAvailableSchedules(branchId, availableDateId, operativeTypeId);
            return new ResponseEntity<>(data, HttpStatus.OK);
        }catch (RetailException e) {
            return new ResponseEntity<>(Utils.buildError(e), HttpStatus.CONFLICT);
        }
    }
}
