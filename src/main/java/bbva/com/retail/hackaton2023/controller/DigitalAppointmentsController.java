package bbva.com.retail.hackaton2023.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bbva.com.retail.hackaton2023.dto.pointments.DataCancelationDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataDigitalAppointmentDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DataListDigitalAppointmentDTO;
import bbva.com.retail.hackaton2023.dto.pointments.DigitalAppointmentDTO;
import bbva.com.retail.hackaton2023.service.DigitalAppPointmentsService;
import bbva.com.retail.hackaton2023.util.RetailException;
import bbva.com.retail.hackaton2023.util.Utils;

@RestController
@RequestMapping("/digital-appointments/v0/digital-appointments")
public class DigitalAppointmentsController {

	@Autowired
    private DigitalAppPointmentsService digitalAppPointmentsService;
	
	@PostMapping
    public ResponseEntity<?> createDigitalAppointment(@RequestBody DigitalAppointmentDTO body) {
        try {
        	DataDigitalAppointmentDTO response = digitalAppPointmentsService.createDigitalAppointment(body);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (RetailException e) {
            return new ResponseEntity<>(Utils.buildError(e), HttpStatus.CONFLICT);
        }
    }
	
	@GetMapping("/{customer-id}")
    public ResponseEntity<?> listDigitalAppointments(@PathVariable("customer-id") String customerId) {
        try {
        	DataListDigitalAppointmentDTO response = digitalAppPointmentsService.listDigitalAppointment(customerId);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (RetailException e) {
            return new ResponseEntity<>(Utils.buildError(e), HttpStatus.CONFLICT);
        }
    }
	
	@DeleteMapping("/{digital-appointment-id}")
    public ResponseEntity<?> deleteDigitalAppointment(@PathVariable("digital-appointment-id") String digitalAppointmentId) {
        try {
        	DataCancelationDTO response = digitalAppPointmentsService.deleteDigitalAppointment(digitalAppointmentId);
            return new ResponseEntity<>(response, HttpStatus.OK);
        }catch (RetailException e) {
            return new ResponseEntity<>(Utils.buildError(e), HttpStatus.CONFLICT);
        }
    }
}
