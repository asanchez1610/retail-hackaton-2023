package bbva.com.retail.hackaton2023.dto.pointments;

import java.util.List;

import lombok.Data;

@Data
public class LocationDTO {

	protected String formattedAddress;
	protected List<AddressComponentDTO> addressComponents;
}
