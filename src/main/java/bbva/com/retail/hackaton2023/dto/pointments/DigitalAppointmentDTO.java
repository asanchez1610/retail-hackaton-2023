package bbva.com.retail.hackaton2023.dto.pointments;

import lombok.Data;

@Data
public class DigitalAppointmentDTO {

	protected String codigoCliente;
	protected String id;
	protected Number operationNumber;
	protected String ticketNumber;
	protected Boolean isActive;
	protected String lastUpdateDate;
	protected String creationDate;
	protected String customer;
	protected String appointmentAttentionDate;
	protected OperativeTypeDTO operativeType;
	protected BankDTO bank;

}
