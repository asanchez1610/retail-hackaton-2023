package bbva.com.retail.hackaton2023.dto.pointments;
import lombok.Data;
import java.util.List;

@Data
public class DataOperativeTypeDTO {

    protected List<OperativeTypeDTO> data;

}
