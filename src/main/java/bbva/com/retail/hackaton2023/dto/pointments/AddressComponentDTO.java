package bbva.com.retail.hackaton2023.dto.pointments;

import java.util.List;

import lombok.Data;

@Data
public class AddressComponentDTO {

	protected String name;
	protected List<String> componentTypes;
}
