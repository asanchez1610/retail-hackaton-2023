package bbva.com.retail.hackaton2023.dto.pointments;

import lombok.Data;

@Data
public class GeolocationDTO {

	protected Number latitude;
	protected Number longitude;
}
