package bbva.com.retail.hackaton2023.dto.pointments;
import lombok.Data;

@Data
public class ContactTypeDTO {

	protected String id;
	protected String name;
}
