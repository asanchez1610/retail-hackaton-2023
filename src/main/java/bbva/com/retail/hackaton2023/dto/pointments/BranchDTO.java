package bbva.com.retail.hackaton2023.dto.pointments;
import lombok.Data;

@Data
public class BranchDTO {

	protected String id;
	protected String name;
	protected LocationDTO location;
	protected GeolocationDTO geolocation;
	protected PhoneDTO phone;
	
}
