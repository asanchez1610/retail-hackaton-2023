package bbva.com.retail.hackaton2023.dto.pointments;

import lombok.Data;

@Data
public class AvailableScheduleDTO {

	protected String id;
	protected String estimatedWaitingTime;
	protected Number number;
	protected Number maximumAttentionsLimit;
	protected Boolean hasCanceledAppointments;
	protected OperativeTypeDTO operativeType;
}
