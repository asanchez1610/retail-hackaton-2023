package bbva.com.retail.hackaton2023.dto;
import bbva.com.retail.hackaton2023.util.TicketManager;
import lombok.Data;

@Data
public class OauthResponseDTO {

    protected String access_token;
    protected Long errorCode;
    protected String message;
    protected Long status;
    protected String token_type;    
    protected String scope;
    protected Long expires_in;
    protected String timestamp;
    protected String path;
    protected String error;
    protected String requestId;
    protected TicketManager ticketManager;

}
