package bbva.com.retail.hackaton2023.dto.pointments;
import lombok.Data;
import java.util.List;

@Data
public class OperativeTypeDTO {

    protected String id;
    protected String name;
    protected String attentionType;
    protected String description;
    protected List<AlternativeChannel> alternativeChannels;

}
