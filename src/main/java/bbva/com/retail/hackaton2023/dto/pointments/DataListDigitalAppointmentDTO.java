package bbva.com.retail.hackaton2023.dto.pointments;

import java.util.List;

import lombok.Data;

@Data
public class DataListDigitalAppointmentDTO {

	protected List<ListDigitalAppointments> data;
	
}
