package bbva.com.retail.hackaton2023.dto.pointments;

import java.util.List;

import lombok.Data;

@Data
public class DataListAvailableScheduleDTO {

	protected List<AvailableScheduleDTO> data;
}
