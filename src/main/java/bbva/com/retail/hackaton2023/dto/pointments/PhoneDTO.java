package bbva.com.retail.hackaton2023.dto.pointments;

import lombok.Data;

@Data
public class PhoneDTO {

	protected String contact;
	protected String description;
	protected ContactTypeDTO contactType;
}
