package bbva.com.retail.hackaton2023.dto.pointments;
import lombok.Data;

@Data
public class AlternativeChannel {

    protected String id;
    protected String name;
    protected Link links;

}
