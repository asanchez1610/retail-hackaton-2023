package bbva.com.retail.hackaton2023.dto.pointments;

import lombok.Data;

@Data
public class CancelationDTO {

	protected String cancelationNumber;
	protected String cancelationDate;
}
